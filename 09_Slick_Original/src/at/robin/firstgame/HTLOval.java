package at.robin.firstgame;

import org.newdawn.slick.Graphics;

public class HTLOval implements Actor {
	private double x,y;
	private int width, height;
	private int statusoval=0;
	private double oval;
	private double delta;

	public HTLOval(double x, double y, int width, int height) {
		super();
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
	}


	public void update(int delta) {
		
			this.y += (double) delta *0.5;
		
			if(this.y>=700) {
				this.y=-10;
			}
		}
	
	
	public void render (Graphics g) {
		g.drawOval((int)this.x,(int)this.y,100, 100);
	}
}
