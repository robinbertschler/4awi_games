package at.robin.firstgame;

import java.util.ArrayList;
import java.util.List;

import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.BasicGame;
import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.tests.AnimationTest;

public class FirstGame extends BasicGame {
	private Starship starship;
	private Bullet bullet;
	private List<Actor> actors;

	public FirstGame() {
		super("First Game");
	}

	@Override
	public void render(GameContainer gameContainer, Graphics g) throws SlickException {

		for (Actor actor : this.actors) {
			actor.render(g);
		}

	}

	@Override
	public void init(GameContainer gameContainer) throws SlickException {
		this.actors = new ArrayList<>();

		for (int i = 0; i < 100; i++) {

			this.actors.add(new HTLSnowflake(0, 0.2));
		}
		for (int i = 0; i < 100; i++) {

			this.actors.add(new HTLSnowflake(1, 0.4));
		}
		for (int i = 0; i < 100; i++) {

			this.actors.add(new HTLSnowflake(2, 0.6));
		}

		MoveStrategy mls = new MoveSideToSide(20, 20);
		this.starship = new Starship(400, 400);
		this.actors.add(new HTLCircle(10, 100, mls));
		this.actors.add(starship);
		this.actors.add(new HTLOval(10, 100, 200, 200));
		this.actors.add(new HTLShootingstar(18, 0.9));
	}

	public void keyPressed(int key, char c) {
		super.keyPressed(key, c);
		if (key == Input.KEY_RIGHT) {
			this.starship.isPressedRight();
		}else if (key == Input.KEY_LEFT) {
			this.starship.isPressedLeft();
		}else if (key == Input.KEY_SPACE) {
		
			this.actors.add(new Bullet(at.robin.firstgame.Starship.getX()+40, at.robin.firstgame.Starship.getY(), 8,10, 1.2));
		}
		
	}

	public void keyReleased(int key, char c) {
		this.starship.isReleased();
	}

	@Override
	public void update(GameContainer gmaeContainer, int delta) throws SlickException {

		for (Actor actor : this.actors) {
			actor.update(delta);
		}

	}

	public static void main(String[] argv) {
		try {
			AppGameContainer container = new AppGameContainer(new FirstGame());
			container.setDisplayMode(800, 600, false);
			container.start();
		} catch (SlickException e) {
			e.printStackTrace();
		}
	}

}
