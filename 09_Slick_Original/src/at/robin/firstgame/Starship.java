package at.robin.firstgame;

import org.newdawn.slick.Graphics;

public class Starship implements Actor {
	private static double x;
	private static double y;
	private boolean isPressedRight;
	private boolean isPressedLeft;
	private boolean isReleased;
	private boolean isMovingRight;

	public Starship(double x, double y) {
		super();
		this.x = x;
		this.y = y;
	}


	public void isPressedRight() {
		this.isPressedRight = true;
	}

	public void isPressedLeft() {
		this.isPressedLeft = true;
	}

	public void isReleased() {
		this.isPressedRight = false;
		this.isPressedLeft = false;
	}

	public void goLeft() {
		this.isMovingRight = false;
	}

	@Override
	public void update(int delta) {
		System.out.println(this.isPressedRight);
		if (this.isPressedRight == true) {
			x++;
		}else if (this.isPressedLeft == true) {
			x--;
		}

	}

	public void render(Graphics g) {
		g.fillRect((int) this.x, (int) this.y, 100, 100);

	}

	public static double getX() {
		return x;
	}

	public void setX(double x) {
		this.x = x;
	}

	public static double getY() {
		return y;
	}

	public void setY(double y) {
		this.y = y;
	}
}
