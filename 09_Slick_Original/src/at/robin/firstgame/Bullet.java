package at.robin.firstgame;

import org.newdawn.slick.Graphics;
import org.newdawn.slick.Color;
public class Bullet implements Actor {
	private double x, y;
	private int width;
	private int height;
	private double speed;
	private boolean shoot = false;

	public Bullet(double x, double y, int width, int height, double speed) {
		super();
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
		this.speed = speed;
	}

	public void isPressedShoot() {
		this.shoot = true;
	}

	public void update(int delta) {
		this.y -= (double) delta * speed;
	}

	public void render(Graphics g) {
		
	
		g.setColor(Color.red);
		g.fillOval((int) this.x, (int) this.y, width, height);
		g.setColor(Color.white);
	}
}
