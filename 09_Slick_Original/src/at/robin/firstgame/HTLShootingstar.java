package at.robin.firstgame;

import java.util.Random;

import org.newdawn.slick.Graphics;
import org.newdawn.slick.Color;

public class HTLShootingstar implements Actor{

	private double x, y;
	private float size;
	private double speed;

	public HTLShootingstar(float size, double speed) {

		this.size =  size;
		this.speed = speed;
		setRandomPosition();
		


		/*if (size == 0) {
			this.size = 5;

		} else if (size == 1) {
			this.size = 8;

		} else {
			this.size = 10;

		}*/

	}

	public void update(int delta) {
		this.x += (double) delta * speed;

	if(this.x>0) {
		
	
	for (int i = 0; i < 500; i++) {
			this.size = (float) (this.size +0.0003);
		}
		if (this.x >= 800) {
			setRandomSize();
			setRandomPosition();
		}
	}
	}

	private void setRandomSize() {
		Random r2 = new Random();
		this.size=r2.nextInt(8);
	}
	private void setRandomPosition() {
		Random r = new Random();
		this.y = r.nextInt(300);
		this.x = r.nextInt(100)-5000;
	}

	public void render(Graphics g) {
		g.setColor(Color.yellow);
		g.drawOval((int) this.x, (int) this.y, size, size);
		
		g.fillOval((int) this.x, (int) this.y, size, size);
		g.setColor(Color.white);
	}


}
