package at.robin.firstgame;

import java.util.Random;

import org.newdawn.slick.Graphics;

public class HTLSnowflake implements Actor{
	private double x, y;
	private int size;
	private double speed;

	public HTLSnowflake(int size, double speed) {

		this.size = size;
		this.speed = speed;
		setRandomPosition();

		if (size == 0) {
			this.size = 5;

		} else if (size == 1) {
			this.size = 8;

		} else {
			this.size = 10;

		}

	}

	public void update(int delta) {
		this.y += (double) delta * speed;

		if (this.y >= 800) {
			setRandomPosition();
		}
	}

	private void setRandomPosition() {
		Random r = new Random();
		this.y = r.nextInt(600) - 600;
		this.x = r.nextInt(800);
	}

	public void render(Graphics g) {
		g.fillOval((int) this.x, (int) this.y, size, size);

	}
}
