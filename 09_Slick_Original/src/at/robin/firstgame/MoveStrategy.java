package at.robin.firstgame;

public interface MoveStrategy{
		public void update(int delta);
		public double getX();
		public double getY();

}
