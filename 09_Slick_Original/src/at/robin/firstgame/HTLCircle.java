package at.robin.firstgame;

import org.newdawn.slick.Graphics;

public class HTLCircle implements Actor {

	private MoveStrategy mls;
	private int width;
	private int height;
	
	
	public HTLCircle(int width, int height, MoveStrategy mls) {
		super();
		this.mls = mls;
		this.width=width;
		this.height=height;
	}

	public void update(int delta) {
		this.mls.update(delta);
	}

	public void render(Graphics g) {
		g.drawOval((int) this.mls.getX(), (int) this.mls.getY(),this.width, this.height);
	}

}
