package at.robin.snailgame;

import java.util.Random;

import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;

public class ObstacleLeft1 implements Actor {
	private double x, y;
	private int width, height;
	private int statusoval = 0;
	private double oval;
	private double delta;
	private double status;
	private Image poison;

	public ObstacleLeft1(double x, double y, int width, int height) {
		super();
		this.x = 580;
		this.y = -600;
		this.width = width;
		this.height = height;
		try {
			this.poison = new Image("testdata/poison.png");
		} catch (SlickException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void update(int delta) {
		this.y += (double) delta * 0.5;

		if ((System.currentTimeMillis() - Main.spawnccoldown342) >= 700
				&& (this.y >= 940)) {
			
			Random r = new Random();
			this.status = r.nextInt(2);
			System.out.println(this.status);
			this.y = -20;
			Main.spawnccoldown342 = System.currentTimeMillis();
			// System.out.println(this.y);

////		 else  {
////			this.y = -1500;
//		//	 System.out.println(this.y);
//		}
		}

	}

	public void render(Graphics g) {
//		g.drawOval((int) this.x, (int) this.y, 20, 20);
		this.poison.draw((int) this.x,(int)this.y,300,300);
	}
}
