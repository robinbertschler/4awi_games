package at.robin.snailgame;

import org.newdawn.slick.Graphics;

public class HTLRectangle implements Actor{
	private double x,y;
	private int width, height;
	public HTLRectangle(double x, double y, int width, int height) {
		super();
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
	}
	
	public void update(int delta) {
		if (this.x <= 600 && this.y <= 50) {
			this.x += (double) delta * 0.5;
		} else if (this.x >= 600 && this.y <= 400) {
			this.y += (double) delta * 0.5;
		} else if (this.x >= 10 && this.y >= 400) {
			this.x += (double) delta * (-1 * 0.5);
		} else if (this.x <= 10 && this.y >= 30) {
			this.y += (double) delta * (-1 * 0.5);
		}
	}
	
	public void render (Graphics g) {
		g.drawRect((int)this.x,(int)this.y,100, 100);
	}
	
}
