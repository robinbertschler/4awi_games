package at.robin.snailgame;

import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;

public class SnailRight implements Actor {
	private static double x;
	private static double y;
	private boolean isPressedRight;
	private boolean isPressedLeft;
	private boolean isReleased;
	private boolean isMovingRight;
	private Image SnailRight;

	public SnailRight(double x, double y) {
		super();
		this.x = 1150;
		this.y = 850;
		try {
			this.SnailRight=new Image("testdata/snail.png");
		} catch (SlickException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}


	public void isPressedRight() {
		this.isPressedRight = true;
	}

	public void isPressedLeft() {
		this.isPressedLeft = true;
	}

	public void isReleased() {
		this.isPressedRight = false;
		this.isPressedLeft = false;
	}

	public void goLeft() {
		this.isMovingRight = false;
	}

	@Override
	public void update(int delta) {
	
		if (this.isPressedRight == true) {
			x=1150;
		}else if (this.isPressedLeft == true) {
			x=1000;
		}

	}

	public void render(Graphics g) {
//		g.fillRect((int) this.x, (int) this.y, 20, 20);
		this.SnailRight.draw((int) this.x, (int)this.y,80,80);
	}

	public static double getX() {
		return x;
	}

	public void setX(double x) {
		this.x = x;
	}

	public static double getY() {
		return y;
	}

	public void setY(double y) {
		this.y = y;
	}
}
