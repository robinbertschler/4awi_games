package at.robin.snailgame;

import java.util.ArrayList;
import java.util.List;

import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.BasicGame;
import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.tests.AnimationTest;

public class Main extends BasicGame {
	private SnailRight starship1;
	private SnailLeft starship2;
	private List<Actor> actors;
	public static long spawnccoldown34;
	public static long spawnccoldown342;
	private Image background;
	public Main() {
		super("First Game");
	}

	@Override
	public void render(GameContainer gameContainer, Graphics g) throws SlickException {
		g.scale(0.5f,0.6f);
		g.drawImage(background, 1320, 0);
		g.resetTransform();
		for (Actor actor : this.actors) {
			actor.render(g);
		}
		
	}

	@Override
	public void init(GameContainer gameContainer) throws SlickException {
		this.actors = new ArrayList<>();

		this.actors.add(new ObstacleRight1(0, 0, 0, 0));
		this.actors.add(new ObstacleRight2(0, 0, 0, 0));

		this.actors.add(new ObstacleLeft1(0, 0, 0, 0));
		this.actors.add(new ObstacleLeft2(0, 0, 0, 0));

		MoveStrategy mls = new MoveSideToSide(20, 20);
		this.starship1 = new SnailRight(400, 400);
		this.starship2 = new SnailLeft(400, 400);

		this.actors.add(starship1);
		this.actors.add(starship2);
		this.background=new Image("testdata/track.png");
		

	}

	public void keyPressed(int key, char c) {
		super.keyPressed(key, c);
		if (key == Input.KEY_RIGHT) {
			this.starship1.isPressedRight();
		} else if (key == Input.KEY_LEFT) {
			this.starship1.isPressedLeft();

		} else if (key == Input.KEY_UP) {
			this.starship2.isPressedRight2();
		}else if (key == Input.KEY_DOWN) {
			this.starship2.isPressedLeft2();

		}
	}

	public void keyReleased(int key, char c) {
		this.starship1.isReleased();
		this.starship2.isReleased();
	}

	@Override
	public void update(GameContainer gmaeContainer, int delta) throws SlickException {
	
		for (Actor actor : this.actors) {
			actor.update(delta);
		}

	}

	public static void main(String[] argv) {
		try {
			AppGameContainer container = new AppGameContainer(new Main());
			container.setDisplayMode(1920, 1080, false);
			container.setTargetFrameRate(60);
			container.start();
	
		} catch (SlickException e) {
			e.printStackTrace();
		}
	}

}
