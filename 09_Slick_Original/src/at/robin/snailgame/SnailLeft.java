package at.robin.snailgame;

import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;

public class SnailLeft implements Actor {
	private static double x;
	private static double y;
	private boolean isPressedRight;
	private boolean isPressedLeft;
	private boolean isReleased;
	private boolean isMovingRight;
	private Image SnailLeft;

	public SnailLeft(double x, double y) {
		super();
		this.x = 700;
		this.y = 850;
		try {
			this.SnailLeft=new Image("testdata/snailgreen.png");
		} catch (SlickException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}


	public void isPressedRight2() {
		this.isPressedRight = true;
	}

	public void isPressedLeft2() {
		this.isPressedLeft = true;
	}

	public void isReleased() {
		this.isPressedRight = false;
		this.isPressedLeft = false;
	}

	public void goLeft() {
		this.isMovingRight = false;
	}

	@Override
	public void update(int delta) {
		
		if (this.isPressedRight == true) {
			x=845;
		}else if (this.isPressedLeft == true) {
			x=700;
		}

	}

	public void render(Graphics g) {
//		g.fillRect((int) this.x, (int) this.y, 20, 20);
		this.SnailLeft.draw((int) this.x, (int)this.y,80,80);
	}

	public static double getX() {
		return x;
	}

	public void setX(double x) {
		this.x = x;
	}

	public static double getY() {
		return y;
	}

	public void setY(double y) {
		this.y = y;
	}
}
