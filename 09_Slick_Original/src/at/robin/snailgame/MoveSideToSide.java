package at.robin.snailgame;

public class MoveSideToSide implements MoveStrategy {
	private double x, y;
	private int statuscirc = 0;

	public MoveSideToSide(double x, double y) {
		super();
		this.x = x;
		this.y = y;
	}
	public void update(int delta){
		if (this.statuscirc == 0) {
			this.x += (double) delta * 0.5;
			if (this.x >= 700) {
				this.statuscirc = 1;
			}
		} else if (this.statuscirc == 1) {
			this.x += (double) delta * (-1 * 0.5);
			if (this.x <= 10) {
				this.statuscirc = 0;
			}
		}
	}
	public double getX() {
		return x;
	}
	public void setX(double x) {
		this.x = x;
	}
	public double getY() {
		return y;
	}
	public void setY(double y) {
		this.y = y;
	}
	
}
